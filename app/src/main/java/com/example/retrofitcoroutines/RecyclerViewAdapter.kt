package com.example.retrofitcoroutines

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.retrofitcoroutines.databinding.ItemLayoutBinding
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou


class RecyclerViewAdapter() : RecyclerView.Adapter<RecyclerViewAdapter.ItemViewHolder>() {

    private val items = mutableListOf<CountryModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView = ItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        val holder = ItemViewHolder(itemView)
        return holder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = items.size


    inner class ItemViewHolder(private val binding: ItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var item: CountryModel
        fun bind() {
            item = items[adapterPosition]
            binding.txVName.text = item.name
            binding.txVCapital.text = item.capital
            loadSVG(item.flag.toString())
        }

        fun loadSVG(url: String) {
            GlideToVectorYou.init().with(binding.root.context).load(Uri.parse(url), binding.imVFlag)
        }
    }

    fun setItems(countries: MutableList<CountryModel>) {
        this.items.clear()
        this.items.addAll(countries)
        notifyDataSetChanged()
    }

    fun clearItems() {
        this.items.clear()
        notifyDataSetChanged()
    }
}