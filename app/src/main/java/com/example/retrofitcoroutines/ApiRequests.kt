package com.example.retrofitcoroutines

import retrofit2.Response
import retrofit2.http.GET

interface ApiRequests {
    @GET("/rest/v2/all")
    suspend fun getCountry(): Response<List<CountryModel>>
}