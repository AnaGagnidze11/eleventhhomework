package com.example.retrofitcoroutines

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val countriesLiveData = MutableLiveData<List<CountryModel>>().apply {
        mutableListOf<CountryModel>()
    }

    val _countriesLiveData: LiveData<List<CountryModel>> = countriesLiveData

    private val loadingLiveData = MutableLiveData<Boolean>()
    val _loadingLiveData: MutableLiveData<Boolean> = loadingLiveData

    fun initCountries(){
        CoroutineScope(Dispatchers.IO).launch {
            getCountries()
        }
    }

    private suspend fun getCountries(){
        loadingLiveData.postValue(true)
        val result = RetrofitService.retrofitService().getCountry()
        if (result.isSuccessful){
            val items = result.body()
            countriesLiveData.postValue(items)
        }

        loadingLiveData.postValue(false)
    }
}