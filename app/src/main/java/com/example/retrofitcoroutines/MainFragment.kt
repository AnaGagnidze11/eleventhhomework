package com.example.retrofitcoroutines

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.retrofitcoroutines.databinding.MainFragmentBinding

class MainFragment : Fragment() {

    private val viewModel: MainViewModel by viewModels()

    private lateinit var binding: MainFragmentBinding

    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MainFragmentBinding.inflate(inflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        viewModel.initCountries()
        initRecyclerView()
        observing()

        binding.refresh.setOnRefreshListener {
            adapter.clearItems()
            viewModel.initCountries()
        }
    }

    private fun initRecyclerView() {
        adapter = RecyclerViewAdapter()
        binding.recyclerView.layoutManager = LinearLayoutManager(requireActivity())
        binding.recyclerView.adapter = adapter
    }

    private fun observing() {

        viewModel._loadingLiveData.observe(viewLifecycleOwner, Observer {
            binding.refresh.isRefreshing = it
        })
        viewModel._countriesLiveData.observe(viewLifecycleOwner, Observer {
            adapter.setItems(it.toMutableList())
        })
    }

}