package com.example.retrofitcoroutines

data class CountryModel(val name: String? = null, val capital: String? = null, val flag: String? = null)
